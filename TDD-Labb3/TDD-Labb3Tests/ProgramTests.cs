﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TDD_Labb3.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void CountVowelsAndConsonantsTestWithSentence()
        {
            //arrange
            const string sentenceToTest = "Hej på dig";
            const int expectedVowels = 3;
            const int expectedConsonats = 5;

            int? resultVowels;
            int? resultConsonants;

            //act
            Program.CountVowelsAndConsonants(sentenceToTest, out resultVowels, out resultConsonants);
            //assert
            Assert.IsTrue(expectedConsonats.Equals(resultConsonants) && expectedVowels.Equals(resultVowels));
        }

        [TestMethod()]
        public void CountVowelsAndConsonantsTestWithWord()
        {
            //arrange
            const string wordToTest = "Hej";
            const int expectedVowels = 1;
            const int expectedConsonats = 2;

            int? resultVowels;
            int? resultConsonants;

            //act
            Program.CountVowelsAndConsonants(wordToTest, out resultVowels, out resultConsonants);
            //assert
            Assert.IsTrue(expectedConsonats.Equals(resultConsonants) && expectedVowels.Equals(resultVowels));
        }

        [TestMethod()]
        public void CountVowelsAndConsonantsTestWithNothing()
        {
            //arrange
            const string sentenceToTest = "";
            const int expectedVowels = 0;
            const int expectedConsonats = 0;

            int? resultVowels;
            int? resultConsonants;

            //act
            Program.CountVowelsAndConsonants(sentenceToTest, out resultVowels, out resultConsonants);
            //assert
            Assert.IsTrue(expectedConsonats.Equals(resultConsonants) && expectedVowels.Equals(resultVowels));
        }

        [TestMethod()]
        public void StringReplacementTestWithSentence()
        {
            //arrange
            const string sentenceToTest = "Hej på dig";
            const string wordToReplace = "på";
            const string wordToReplaceWith = "ifrån";

            const string expected = "Hej ifrån dig";
            //act
            var result = Program.StringReplacement(sentenceToTest, wordToReplace, wordToReplaceWith);
            //assert
            Assert.IsTrue(result.Equals(expected));
        }

        [TestMethod()]
        public void StringReplacementTestWithWord()
        {
            //arrange
            const string wordToTest = "Hej";
            const string wordToReplace = "Hej";
            const string wordToReplaceWith = "ifrån";

            const string expected = "ifrån";
            //act
            var result = Program.StringReplacement(wordToTest, wordToReplace, wordToReplaceWith);
            //assert
            Assert.IsTrue(result.Equals(expected));
        }

        [TestMethod()]
        public void StringReplacementTestWithNothing()
        {
            //arrange
            const string sentenceToTest = "";
            const string wordToReplace = "";
            const string wordToReplaceWith = "";

            const string expected = "";
            //act
            var result = Program.StringReplacement(sentenceToTest, wordToReplace, wordToReplaceWith);
            //assert
            Assert.IsTrue(result.Equals(expected));
        }
    }
}